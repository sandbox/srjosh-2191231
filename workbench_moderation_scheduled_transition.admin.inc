<?php

/**
 * @file
 * Administrative functions for Workbench Moderation Scheduled Transitions.
 */

/**
 * Page callback for scheduled transition listings, per node.
 *
 * @param obj $node
 *   Node object for which to display scheduled listings.
 */
function workbench_moderation_scheduled_transition_listing($node) {
  global $user;

  // Page title.
  drupal_set_title(t('Scheduled transitions for %title', array('%title' => $node->title)), PASS_THROUGH);

  $s_trans = workbench_moderation_scheduled_transition_by_node($node);
  $rows = array();
  $header = array(
    t('Scheduled Time'),
    t('Transition'),
    '',
  );

  foreach ($s_trans as $transition) {
    $trans = explode(":", $transition->transition);
    switch ($trans[0]) {
      case 'unpublish':
        $trans_text = "Unpublish to " . $trans[1];
        break;

      case 'archive':
        $trans_text = "Archive";
        break;

      case 'unarchive':
        $trans_text = "Un-archive";
        break;

      default:
      case 'transition':
// TODO: make this not reliant on wm_profile
        $profile_load = workbench_moderation_profile_transition_load($trans[1]);
        $trans_text = $profile_load->name;
        break;
    }

    $rows[] = array(
      format_date($transition->stamp, 'medium'),
      $trans_text,
      l(t('edit'), 'node/' . $node->nid . '/wmst_scheduler/' . $transition->wmstid . '/edit') . ' | ' .
      l(t('delete'), 'node/' . $node->nid . '/wmst_scheduler/' . $transition->wmstid . '/delete'),
    );
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));
  return $output;
}

/**
 * Form constructor for scheduled transition form.
 *
 * @see workbench_moderation_scheduled_transition_form_submit()
 * @see workbench_moderation_scheduled_transition_form_validate()
 *
 * @ingroup forms
 */
function workbench_moderation_scheduled_transition_form($form, &$form_state, $node, $wmstid = NULL) {
  $options = array();
// TODO: make this not reliant on wm_profile
  // Load profile and profile's transitions.
  $profile = workbench_moderation_profile_get_profile_from_node($node);
  $profile = workbench_moderation_profile_load($profile);
  $transitions = field_get_items('workbench_moderation_profile', $profile, 'field_transitions', $profile);
  // Add all approved transitions into option list.
  foreach ($transitions as $transition) {
    $tran = workbench_moderation_profile_transition_load($transition['target_id']);
    $options["transition:" . $tran->id] = $tran->name . ' (' . $tran->from_name . ' --> ' . $tran->to_name . ')';
  }

// TODO: make this not reliant on wm_profile
  // Load all states from profile.
  $states = workbench_moderation_profile_get_states($profile);
  // Add Unpublish actions to option list.
  unset($states['published']);
  foreach ($states as $state => $text) {
    $options['unpublish:' . $state] = "Unpublish (published -->" . $state . ')';
  }

  // Open Atrium uses trash_flag.module to handle an "archive" flag.
  if (module_exists('trash_flag')) { 
    $options['archive'] = "Archive (published --> archived)";
    $options['unarchive'] = "Un-archive (archived --> published)";
  }

  $strans = workbench_moderation_scheduled_transition_load($wmstid);

  $form['wmstid'] = array(
    '#type' => 'value',
    '#value' => $wmstid,
  );

  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => 'node',
  );

  $form['workbench_moderation_scheduled_transition_transition'] = array(
    '#type' => 'select',
    '#title' => t('Transition to enact'),
    '#description' => t('The transition to perform on this node at the scheduled time.'),
    '#options' => $options,
    '#default_value' => isset($strans['transition']) ? $strans['transition'] : 0,
  );

  $form['workbench_moderation_scheduled_transition_stamp'] = array(
    '#type' => 'date_popup',
    '#title' => t('Scheduled Date'),
    '#default_value' => isset($strans['stamp']) ? date('Y-m-d H:i', $strans['stamp']) : 0,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

/**
 * Form validation handler for scheduled transition form.
 */
function workbench_moderation_scheduled_transition_form_validate($form, &$form_state) {

}

/**
 * Form submission handler for scheduled transition form.
 */
function workbench_moderation_scheduled_transition_form_submit($form, &$form_state) {
  $vals = $form_state['values'];

  $form_state['redirect'] = array("node/" . $vals['entity_id'] . "/wmst_scheduler");

  if (!empty($vals['workbench_moderation_scheduled_transition_stamp'])) {
    $record = new stdClass();
    $record->entity_id = $vals['entity_id'];
    $record->entity_type = $vals['entity_type'];
    $record->stamp = strtotime($vals['workbench_moderation_scheduled_transition_stamp']);
    $record->transition = $vals['workbench_moderation_scheduled_transition_transition'];
    if (isset($vals['wmstid'])) {
      $record->wmstid = $vals['wmstid'];
      $success = drupal_write_record('workbench_moderation_scheduled_transitions', $record, array('wmstid'));
    }
    else {
      $success = drupal_write_record('workbench_moderation_scheduled_transitions', $record);
    }

    if ($success) {
      drupal_set_message(t("Transition scheduled for @date", array('@date' => $vals['workbench_moderation_scheduled_transition_stamp'])), 'status');
    }
  }
}

/**
 * Form constructor for scheduled transition deletion form.
 *
 * @see workbench_moderation_scheduled_transition_delete_form_submit()
 *
 * @ingroup forms
 */
function workbench_moderation_scheduled_transition_delete_form($form, &$form_state, $node, $wmstid = NULL) {
  $form['#node'] = $node;
  $form['wmstid'] = array(
    '#type' => 'value',
    '#value' => $wmstid,
  );
  $s_trans = workbench_moderation_scheduled_transition_load($wmstid);
// TODO: make this not reliant on wm_profile
  $trans = workbench_moderation_profile_transition_load($s_trans['transition']);

  return confirm_form($form,
    t('Are you sure you wish to delete the transition scheduled for %date?', array(
      '%date' => format_date($s_trans['stamp'], 'medium'))
    ),
    'node/' . $node->nid . '/wmst_scheduler'
  );
}

/**
 * Form submission handler for scheduled transition deletion form.
 */
function workbench_moderation_scheduled_transition_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    workbench_moderation_scheduled_transition_delete($form_state['values']['wmstid']);
    drupal_set_message(t('Scheduled transition deleted.'));
  }
  $form_state['redirect'] = 'node/' . $form['#node']->nid . '/wmst_scheduler';
}
