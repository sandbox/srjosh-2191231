<?php
/**
 * @file
 * Drush command definitions for Workbench Moderation Scheduled Transition.
 */

/**
 * Implements hook_drush_help().
 */
function workbench_moderation_scheduled_transition_drush_help($section) {
  switch ($section) {
    case 'drush:workbench-moderation-scheduled-transition':
      return dt('Transition nodes from one state to another in bulk.');

    case 'meta:workbench_moderation_scheduled_transition:title':
      return dt("Workbench Moderation Scheduled Transitions");

    case 'meta:workbench_moderation_scheduled_transition:summary':
      return dt("Allows for transitions of nodes from one state to the next in bulk.");
  }
}

/**
 * Implements hook_drush_command().
 */
function workbench_moderation_scheduled_transition_drush_command() {
  $items['workbench-moderation-scheduled-transition-by-node'] = array(
    'drupal dependencies' => array('workbench_moderation'),
    'callback' => 'workbench_moderation_scheduled_transition_drush_by_node',
    'description' => 'Perform all scheduled operations with timestampe <= now.',
    'aliases' => array('wmbt-node'),
  );

  $items['workbench-moderation-scheduled-transition'] = array(
    'drupal dependencies' => array('workbench_moderation'),
    'callback' => 'workbench_moderation_scheduled_transition_do',
    'description' => 'Transition nodes from one state to another. This will transition all nodes (filtered by options) in the designated "from" state to the designated "to" state.  Appropriate handling for publish/unpublishing operations is included.',
    'aliases' => array('wmbt'),
    'arguments' => array(
      'from_name' => 'Machine name of the starting state',
      'to_name' => 'Machine name of the ending state',
    ),
    'options' => array(
      'content_type' => 'Comma-delimited list of machine names of the node bundle',
      'gid' => 'Group ID of the organic group (requires OG to be enabled)',
      'before' => 'Node workflow transition date by which to filter (<=) - Unix timestamp (UTC)',
      'after' => 'Node workflow transition date by which to filter (>=) - Unix timestamp (UTC)',
      'before_relative' => 'Node workflow transition date by which to filter (<=). Expressed as an integer representing difference in seconds from current time. May be a positive number (future date) or negative (past date).',
      'after_relative' => 'Node workflow transition date by which to filter (>=). Expressed as an integer representing difference in seconds from current time. May be a positive number (future date) or negative (past date).',
    ),
    'examples' => array(
      '$ drush workbench-moderation-scheduled-transition draft needs_review' =>
      'Transition nodes from "draft" state to "needs review" state.',
      '$ drush wmbt needs_review published --content_type=article,page' =>
      'Transition article and page nodes from "needs review" state to "published" state; perform all additional publication functions (node->status == 1).',
      '$ drush wmbt published draft --gid=3' =>
      'Transition nodes in Organic Group 3 from "published" state to "draft"; perform all unpublish functions (node->status == 0)',
      '$ drush wmbt published draft --gid=3 --content_type=article --before=1391456765' =>
      'Transition all article nodes in Organic Group 3 with an updated date less than or equal to Feb. 3, 2014, 13:46:05 from "published" state to "draft"; perform all unpublish functions (node->status == 0)',
      '$ drush wmbt needs_review published --gid=37 --content_type=page --after_relative=-604800' =>
      'Transition all page nodes in Organic Group 37 with an updated date greater than or equal to 7 days ago (60 seconds * 60 minutes * 24 hours * 7 days) from "needs review" state to "published"; perform all publish functions (node->status == 1)',
    ),
  );

  $items['workbench-moderation-scheduled-transition-archive'] = array(
    'drupal dependencies' => array('trash_flag', 'flag'),
    'callback' => 'workbench_moderation_scheduled_transition_archive',
    'description' => 'Flag nodes for archived status, using trash_flag module. This will flag all published nodes (filtered by options) to be archived. At least one filter option (not counting operation) is required to prevent you from inadvertently archiving all the things.',
    'aliases' => array('wmbta'),
    'options' => array(
      'operation' => '"flag" or "unflag" - whether to flag this as an archived node, or unarchive it.  Defaults to "flag".',
      'content_type' => 'Comma-delimited list of machine names of the node bundle',
      'gid' => 'Group ID of the organic group (requires OG to be enabled)',
      'before' => 'Node workflow transition date by which to filter (<=) - Unix timestamp (UTC)',
      'after' => 'Node workflow transition date by which to filter (>=) - Unix timestamp (UTC)',
      'before_relative' => 'Node workflow transition date by which to filter (<=). Expressed as an integer representing difference in seconds from current time. May be a positive number (future date) or negative (past date).',
      'after_relative' => 'Node workflow transition date by which to filter (>=). Expressed as an integer representing difference in seconds from current time. May be a positive number (future date) or negative (past date).',
    ),
    'examples' => array(
      '$ drush workbench-moderation-scheduled-transition-archive --content_type=article,page' =>
      'Archive all published article and page nodes.',
      '$ drush wmbta --gid=3 --operation=unflag' =>
      'Unarchive all published nodes in Organic Group 3.',
      '$ drush wmbta --gid=3 --content_type=article --before=1391456765' =>
      'Archive all published nodes in Organic Group 3 with an updated date less than or equal to Feb. 3, 2014, 13:46:05.',
      '$ drush wmbta --gid=37 --content_type=page --after_relative=-604800' =>
      'Archive all published nodes in Organic Group 37 with an updated date greater than or equal to 7 days ago (60 seconds * 60 minutes * 24 hours * 7 days).',
    ),
  );
  return $items;
}

/**
 * Implementation of 'workbench-moderation-scheduled-transition-by-node'.
 */
function workbench_moderation_scheduled_transition_drush_by_node() {
  $query = workbench_moderation_scheduled_transition_get_nodes();
  $result = $query->execute();
  foreach ($result as $record) {
    $operation = explode(":", $record->transition);

    switch ($operation[0]) {
      case 'unpublish':
        workbench_moderation_scheduled_transition_unpublish($operation[1], $record->entity_id);
        break;

      case 'archive':
        // Open Atrium uses trash_flag.module to handle an "archive" flag.
        if (module_exists('trash_flag')) { 
          flag('flag', 'trash', $record->entity_id);
        }
        break;

      case 'unarchive':
        // Open Atrium uses trash_flag.module to handle an "archive" flag.
        if (module_exists('trash_flag')) { 
          flag('unflag', 'trash', $record->entity_id);
        }
        break;

      case 'transition':
        // Load transition, to get states from the transition. // TODO: make this not reliant on wm_profile
        $states = workbench_moderation_profile_transition_load($operation[1]);
        // Load node_history to find vid of node (needed to properly moderate).
        $vid = db_select('workbench_moderation_node_history', 'wmnh')
         ->fields('wmnh', array('vid'))
         ->condition('wmnh.nid', $record->entity_id, '=')
         ->condition('wmnh.state', $states->from_name, '=')
         ->condition('wmnh.current', 1, '=')
         ->execute()->fetchField();

        $node = node_load($record->entity_id, $vid);
        if (is_object($node)) {
          workbench_moderation_moderate($node, $states->to_name);
        }
        break;
    }
    workbench_moderation_scheduled_transition_delete($record->wmstid);
  }
}

// Drush Bulk Commands.
/**
 * Implementation of 'workbench-moderation-scheduled-transition' command.
 *
 * @param string $from_name
 *   The original moderation state.
 * @param string $to_name
 *   The new moderation state.
 */
function workbench_moderation_scheduled_transition_do($from_name, $to_name) {
  $query = workbench_moderation_scheduled_transition_get_query($from_name);
  if (!drush_get_error()) {
    $result = $query->execute();
    return workbench_moderation_scheduled_transition_handler($from_name, $to_name, $result);
  }
}

/**
 * Implementation of 'workbench-moderation-scheduled-transition-archive'.
 */
function workbench_moderation_scheduled_transition_archive() {
  // Open Atrium uses trash_flag.module to handle an "archive" flag.
  if (module_exists('trash_flag')) { 
    // Archive requires at least one option to filter.
    $option_count = 0;
    $query = workbench_moderation_scheduled_transition_get_query('published', $option_count);
    if (!drush_get_error() && $option_count) {
      $result = $query->execute();
      $operation = drush_get_option('operation', 'flag');
      return workbench_moderation_scheduled_transition_archiver($result, $operation);
    }
  }
}
